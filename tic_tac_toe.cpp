#include "player.h"
#include "tic_tac_toe.h"

bool full_board = 0;

void tic::draw_border() {
	for (int i = 0; i < m; i++)
		board[0][i] = 'u';
	for (int i = 1; i < m; i++)
		board[i][0] = '|';
	for (int i = 1; i < m; i++)
		board[i][m-1] = '|';
	for (int i = 0; i < m; i++)
		board[m-1][i] = 'n';
}

void tic::clear_board() {
	for (int i = 1; i < m-1; i++) {
		for (int j = 1; j < m-1; j++) {
			board[i][j] = ' ';
		}
	}
}

tic::tic() {
	board = new char* [m];
	for (int i = 0; i < m; i++)
		board[i] = new char[m];
	draw_border();
}

void tic::display() {
	std::cout << std::endl;
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < m; j++) {
			std::cout << " " << board[i][j];
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

int tic::leader() {
	if (human_player.getScore() > computer_player.getScore())
		return human_player.getScore();
	if (human_player.getScore() < computer_player.getScore())
		return computer_player.getScore();
	else
		return human_player.getScore();
}

void tic::draw(int row, int col, char t) {
	for (int i = 0; i < m; i++) {
		for (int j = 0; j < m; j++) {
			if (i == row && j == col)
				board[row][col] = t;
		}
	}
}

void tic::coordinates(int& row, int& col) {
	int position;
	std::cout << "Vas potez! Odaberite unos (num lock): ";
	std::cin >> position;
	if (position == 7) { row = 1, col = 1; }
	if (position == 8) { row = 1, col = 2; }
	if (position == 9) { row = 1, col = 3; }
	if (position == 4) { row = 2, col = 1; }
	if (position == 5) { row = 2, col = 2; }
	if (position == 6) { row = 2, col = 3; }
	if (position == 1) { row = 3, col = 1; }
	if (position == 2) { row = 3, col = 2; }
	if (position == 3) { row = 3, col = 3; }
}

bool tic::empty(int row, int col) {
	if (board[row][col] == human_player.getTac() || board[row][col] == computer_player.getTac())
		return 0;
	else
		return 1;
}

void tic::human_input() {
	int row, col;
	coordinates(row, col);
	if (!empty(row, col)) {
		std::cout << "Greska! Ponovite unos. " << std::endl;
		human_player.set_turn(1);
	}
	else
		draw(row, col, human_player.getTac());
}

void tic::computer_input() { 
	//napad
	for (int i = 1; i < m - 1; i++) {
		if (board[1][i] == computer_player.getTac() && board[3][i] == computer_player.getTac() && empty(2, i)) {
			draw(2, i, computer_player.getTac());
			return;
		}
		if (board[i][1] == computer_player.getTac() && board[i][3] == computer_player.getTac() && empty(i, 2)) {
			draw(i, 2, computer_player.getTac());
			return;
		}
	}
	if (board[1][1] == computer_player.getTac() && board[3][3] == computer_player.getTac() && empty(2, 2)) {
		draw(2, 2, computer_player.getTac());
		return;
	}
	if (board[3][1] == computer_player.getTac() && board[1][3] == computer_player.getTac() && empty(2, 2)) {
		draw(2, 2, computer_player.getTac());
		return;
	}
	//obrana
	for (int i = 1; i < m-1; i++) {
		if (board[1][i] == human_player.getTac() && board[3][i] == human_player.getTac() && empty(2, i)) {
			draw(2, i, computer_player.getTac());
			return;
		}
		if (board[i][1] == human_player.getTac() && board[i][3] == human_player.getTac() && empty(i, 2)) {
			draw(i, 2, computer_player.getTac());
			return;
		}
	}
	if (board[1][1] == human_player.getTac() && board[3][3] == human_player.getTac() && empty(2, 2)) {
		draw(2, 2, computer_player.getTac());
		return;
	}
	if (board[3][1] == human_player.getTac() && board[1][3] == human_player.getTac() && empty(2, 2)) {
		draw(2, 2, computer_player.getTac());
		return;
	}

	if (empty(1, 1))
		draw(1, 1, computer_player.getTac());
	else if (empty(3, 3))
		draw(3, 3, computer_player.getTac());
	else if (empty(3, 1))
		draw(3, 1, computer_player.getTac());
	else if (empty(1, 3))
		draw(1, 3, computer_player.getTac());
	else if (empty(2, 2))
		draw(2, 2, computer_player.getTac());
	else if (empty(1, 2))
		draw(1, 2, computer_player.getTac());
	else if (empty(2, 1))
		draw(2, 1, computer_player.getTac());
	else if (empty(2, 3))
		draw(2, 3, computer_player.getTac());
	else if (empty(3, 2))
		draw(3, 2, computer_player.getTac());
	else
		full_board = 1;
}

bool tic::check_win(char t) {
	for (int i = 0; i < m; i++) {
		if (board[1][i] == t && board[2][i] == t && board[3][i] == t)
			return 1;
		if (board[i][1] == t && board[i][2] == t && board[i][3] == t)
			return 1;
	}
	if (board[1][1] == t && board[2][2] == t && board[3][3] == t)
		return 1;
	if (board[3][1] == t && board[2][2] == t && board[1][3] == t)
		return 1;
	else
		return 0;
}

bool tic::win() {
	if (full_board == 1) {
		full_board = 0;
		clear_board();
		return 1;
	}
	if (check_win(human_player.getTac())) {
		human_player.increaseScore();
		std::cout << "	Pobjedio je korisnik! " << "(" << human_player.getScore() << ")"<< std::endl;
		return 1;
	}
	if (check_win(computer_player.getTac())) {
		computer_player.increaseScore();
		std::cout << "	Pobjedilo je racunalo! " << "(" << computer_player.getScore() << ")" << std::endl;
		return 1;
	}
	else
		return 0;
}

void tic::game() {
	int lead = leader();
	while (lead < 3) {
		clear_board();
		while (!win()) {
			display();
			if (human_player.whos_turn()) {
				human_player.set_turn(0);
				human_input();
			}
			else {
				std::cout << "Potez racunala. ";
				computer_input();
				human_player.set_turn(1);
			}
		}
		display();
		lead = leader();
	}
	std::cout << "Racunalo: " << computer_player.getScore() << std::endl;
	std::cout << "Korisnik: " << human_player.getScore() << std::endl;
	std::cout << "Gotova igra. " << std::endl;
}

tic::~tic() {
	for (int i = 0; i < m; i++)
		delete[] board[i];
	delete[] board;
	board = NULL;
}