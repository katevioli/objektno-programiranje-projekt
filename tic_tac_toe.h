#include <iostream>

class tic {
	char** board;
	int m = 5;
	human human_player;
	computer computer_player;
public:
	void draw_border();
	void clear_board();
	tic();
	void display();
	int leader();
	void draw(int col, int row, char t);
	void coordinates(int& row, int& col);
	bool empty(int row, int col);
	void human_input();
	void computer_input();
	bool check_win(char t);
	bool win();
	void game();
	~tic();
};