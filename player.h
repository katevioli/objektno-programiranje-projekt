#include <iostream>

class player {
	int score;
	char tac;
public:
	void setTac(char t) { tac = t; };
	char getTac() { return tac; };
	void increaseScore() { score++; };
	int getScore() { return score; };
};

class human : public player {
	bool turn;
public:
	human() { setTac('x'); turn = 1; };
	void set_turn(bool t) { turn = t; };
	bool whos_turn() { return turn; };
};

class computer : public player {
public:
	computer() { setTac('o'); };
};